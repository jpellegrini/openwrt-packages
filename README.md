# Packages for LibreCMC and OpenWRT (mostly Lisps)

These are `Makefiles` that can be used to build packages
of Lisp interpreters for [LibreCMC](https://librecmc.org/) and [OpenWRT](http://openwrt.org/).

This is mostly for experimentation. It would be interesting if some packages would  be integrated
into the LibreCMC or OpenWRT package feed later. Others probably won't.

It may be interesting to set up an [external root](https://openwrt.org/docs/guide-user/additional-software/extroot_configuration), 
so there will be plenty of room for packages.

For small devices without support for external storage (not even a USB stick),
the smallest packages are PicoLisp and TinyScheme.

*Please do check the issue tracker before building the packages!*

See [instructions.md](instructions.md).

**Note:** In several cases, you need to have the same package locally
installed on the host where you are cross compiling it. This is
because several of these will generate a binary and then run it in the
rest of the compilation. This likely means that these will never
actually be part of the LibreCMC / OpenWRT official package set, since
it would be insanely hard to cross-compile cleanly in the build root
without external dependencies (which is what is needed for inclusion).

**Note:** Neil van Dyke 
[has packaged Racket](https://www.neilvandyke.org/racket-openwrt/) for 
OpenWRT, although he mentions that he is not supporting those packages 
at this time.

## Example

Below is an example of two interpreters running on a wireless router.

```
$ ssh root@router
root@router's password: 


BusyBox v1.30.1 () built-in shell (ash)

  _______                     ________        __
 |       |.-----.-----.-----.|  |  |  |.----.|  |_
 |   -   ||  _  |  -__|     ||  |  |  ||   _||   _|
 |_______||   __|_____|__|__||________||__|  |____|
          |__| W I R E L E S S   F R E E D O M
 -----------------------------------------------------
 OpenWrt SNAPSHOT, r9547-364ab34
 -----------------------------------------------------
root@shams:~# csi

CHICKEN
(c) 2008-2020, The CHICKEN Team
(c) 2000-2007, Felix L. Winkelmann
Version 5.2.0 (rev 317468e4)
linux-unix-gnu-mips [ dload ptables ]

Type ,? for help.
#;1> (begin (display "Hello, embedded world!") (newline))
Hello, embedded world!
#;2> 
root@shams:~# stklos
  \    STklos version 1.50  (Id: 3f5fecfd33)
   \   Copyright (C) 1999-2020 Erick Gallesio <eg@unice.fr>
  / \  Université Côte d'Azur
 /   \ [Linux-5.9.0-1-amd64-x86_64/pthread/no-readline/utf8]
stklos> +
#[primitive +]
stklos> (require "srfi-180")
"srfi-180"
stklos>
root@shams:~# bwbasic
########  ##    ## ##      ##    ###    ######## ######## ########
##     ##  ##  ##  ##  ##  ##   ## ##      ##    ##       ##     ##
##     ##   ####   ##  ##  ##  ##   ##     ##    ##       ##     ##
########     ##    ##  ##  ## ##     ##    ##    ######   ########
##     ##    ##    ##  ##  ## #########    ##    ##       ##   ##
##     ##    ##    ##  ##  ## ##     ##    ##    ##       ##    ##
########     ##     ###  ###  ##     ##    ##    ######## ##     ##


                                    ########     ###     ######  ####  ######
                                    ##     ##   ## ##   ##    ##  ##  ##    ##
                                    ##     ##  ##   ##  ##        ##  ##
                                    ########  ##     ##  ######   ##  ##
                                    ##     ## #########       ##  ##  ##
                                    ##     ## ##     ## ##    ##  ##  ##    ##
                                    ########  ##     ##  ######  ####  ######

Bywater BASIC Interpreter, version 3.20
Copyright (c) 1993, Ted A. Campbell
Copyright (c) 1995-1997, Jon B. Volkoff
Copyright (c) 2014-2017, Howard Wulf, AF5NE

bwBASIC: 10 PRINT "Hello from BASIC!"
bwBASIC: run
Hello from BASIC!
bwBASIC: list
   10 PRINT "Hello from BASIC!"

bwBASIC:


root@shams:~# 
```



## Status

|     | version in this repository | upstream version | note |
| --- | --- | --- | --- |
| [Algol 68 Genie](https://jmvdveer.home.xs4all.nl/en.algol-68-genie.html) | 2.8.4 | ***3.5.4 (2024)*** | |
| [bwBASIC](https://sourceforge.net/projects/bwbasic/) | 3.20 | ***3.30 (2024)*** | |
| [Chibi Scheme](http://synthcode.com/scheme/chibi) | 0.11 | 0.11 | see below |
| [Chicken Scheme](http://call-cc.org)  | 5.4.0 | 5.4.0 (2024) | merged 5.2.0: [OpenWRT master](https://github.com/openwrt/packages/pull/8460) and [LibreCMC](https://gogs.librecmc.org/libreCMC/package-feed/pulls/5) |
| [Foment Scheme](https://github.com/leftmike/foment) | 20220428 | 20220428 | |
| [Gauche Scheme](http://practical-scheme.net/gauche) | 0.9.15 | 0.9.15 | |
| [mLite](http://t3x.org/mlite/) | 20141229 | 20141229 | |
| [PForth](http://www.softsynth.com/pforth/) | 20220327 | ***2.0.1 (2023)*** | see below |
| [rlwrap](https://github.com/hanslub42/rlwrap) | 0.46.1 | 0.46.1 | need to patch OpenWRT (see below) |
| [STklos](https://www.stklos.net/) | 2.10 | 2.10 | |
| [TinyScheme](http://tinyscheme.sourceforge.net/home.html) | 1.41 | ***1.42 (2020)*** | |
| [Twin](https://github.com/cosmos72/twin) | 0.9.0 | 0.9.0 | |

| library | version in this repository | upstream version | note |
| --- | --- | --- | --- |
| [libatomic\_ops](https://github.com/ivmai/libatomic_ops) | 7.8.2 | 7.8.2 | Needed for Gauche |
| [libsigsegv](https://www.gnu.org/software/libsigsegv/) | 2.14 | 2.14 | MIPS only |
| [libgc](https://www.hboehm.info/gc/) | 8.2.8 | 8.2.8 | |

Currently available packages on this repository:

* `libatomic_ops` - atomic operations library, needed to compile Gauche's included
   GC. That is the standard libgc, but Gauche compiles it with different configure options,
   so it needs libatomic. Since I could not make it work with GCC's included libatomic
   (it is not used when cross-compiling), and `libatomic_ops` is written by the maintainer
   of `libgc`, this one was packaged, and Gauche works with it.

* `libsigsegv` (MIPS only) and `libgc`

* [Algol 68 Genie](https://jmvdveer.home.xs4all.nl/en.algol-68-genie.html) -- **yes**,
  an [Algol 68](http://www.algol68.org/) implementation, by Marcel van der Veer.
  The Algol 68 Genie project preserves and promotes Algol 68 out of educational as
  well as scientific-historical interest.
  This is a fast compiler-interpreter which ranks among the most complete implementations
  of the language.

  **NOTE:** see the [Algol68](http://www.algol68.org/) site for more information on
  the language.

  **NOTE:** versoin 3.0 seems to require algol68genie installed in order to cross-compile,
  so I'll try to do the same that was done with STklos (build and install a temporary
  version in the buildroot, and use it to cross-compile)

* [bwBASIC](https://sourceforge.net/projects/bwbasic/), the Bywater BASIC Interpreter, which 
  implements a large superset of the ANSI Standard for Minimal BASIC (X3.60-1978) and a 
  significant subset of the ANSI Standard for Full BASIC (X3.113-1987).
  
  The original package was slightly changed (ran `dos2unix` and `chmod a+x` on the configure script,
  and also changed the `ZIP` into a `tar.gz` file which does not unpack into the
  current dir.

* [Chibi Scheme](http://synthcode.com/scheme/chibi) is a small R7RS
  Scheme implementation by Alex Shinn. Chibi has support for a large amount of SRFIs, 
  plus other extensions.

  **NOTE:** for compiling Chibi you need to have the same version installed on
  the compiling machine.

  **NOTE II** `chibi-ffi` and `chibi-doc` are not included

* [Chicken Scheme](http://call-cc.org) is a Scheme implementation,
  originally writtenby Felix Winkelmann and now maintained by the 
  Chicken team. Chicken has support for a large amount of SRFIs and 
  other extensions, and is a very fast interpreter. Supports full
  R5RS, and R7RS support is being worked on.

  Chicken Scheme was first released in 2000.

  **NOTE**: if compilation breaks with `execvp` complaining about the
  size of an argument list, try increasing the stack size limit with
  `ulimit -s 65536`.

  **NOTE, II**: there are two packages, `chicken-scheme-interpreter`, containing
  `csi` only, and `chicken-scheme-full`, with a full chicken installation
  (except it does not include the static library)
  
  **NOTE, III**: `chicken-scheme-full` requires `gcc` and `coreutils-install`.
  
  **NOTE, IV**: if you run long/heavy compilation sessions on a small device,
  memory and storage space may be exhausted, leading to unpredictable behavior.

  **NOTE, V**: since `chicken-scheme-full` depends on `gcc`, be aware that both will
  together require over 100Mb of storage space.

* [Foment Scheme](https://github.com/leftmike/foment)
  Foment is an implementation of R7RS Scheme. It is very small, supporting native threads,
  synchronization primitives, networking, and a number of SRFIs, including 1, 14,
  60, 106, 111, 112, 124, 125, 128, 133, 176, 181 and 192.

* [Gauche Scheme](http://practical-scheme.net/gauche) is an R7RS Scheme implementation developed to be a handy script interpreter, which allows
  programmers and system administrators to write small to large scripts for their daily chores.
  Gauche features a quick startup, built-in system interface, native multilingual support, an
  object system, networking, native multithreading, an XML parser, and a system interface covering
  most of POSIX.1.

  Gauche is a large package (around 10Mb), but loaded with features.

  The binary is *not* called `gauche`, but `gosh`.

  **NOTE:** for compiling Gauche you need to have the same version installed on the compiling machine.

  **NOTE II:** Gauche depends on `libatomic_ops` and `libmbedtls`

  **NOTE III:** The Gauche binary includes full `libgc`. According to the developer
  of Gauche, this is because `libgc` needs to be compiled with specific configure
  options, so linking with a dynamic version of `libgc` doesn't work.

* [mLite](http://t3x.org/mlite/) is a general-purpose, functional, lightweight,
  dynamic programming language. It borrows ideas
  from both the Scheme R4RS and Standard ML
  DEFSML languages, taking most of its syntax
  from ML and its dynamic nature from Scheme. It
  extends ML-style pattern matching by adding
  guarded patterns and also introduces the principle
  of implicit guards. The implementation presented
  here is intended to be portable and easily realized
  on top of existing Scheme systems.
  
* [PForth](http://www.softsynth.com/pforth/) is a portable ANS Forth based on a kernel written in ANSI 'C'. 
  PForth Features:
  * ANS standard support for Core, Core Extensions, File-Access, Floating-Point, Locals, Programming-Tools, Strings word sets.
  * Compiles from simple ANSI 'C' code with no special pre-processing needed. Also compiles under C++.
  * INCLUDE reads source from normal files, not BLOCKs.
  * Precompiled dictionaries can be saved and reloaded.
  * Custom 'C' code can be easily linked with pForth.
  * Handy words like ANEW  INCLUDE? SEE  WORDS.LIKE  FILE?
  * Single Step Debugger
  * Smart conditionals.  10 0 DO I . LOOP works in outer interpreter.
  * Conditional compilation.  [IF]   [ELSE]   [THEN]
  * Local variables using { }
  * 'C' like structure defining words.
  * Vectored execution using DEFER

  **NOTE:** unfortunately I could not find a way to cross-build the pforth dictionary
  except for copying the source system (`.fth` files) into `/tmp/pforth-install/fth`,
  sourcing them to build the dictionary and then deleting them during the pre-install
  phase. If installation is interrupted and the device is powered down, reconfiguring
  will not work; it may be necessary to reinstall the package.

* [rlwrap](https://github.com/hanslub42/rlwrap) is a 'readline wrapper', a small utility 
  that uses the GNU Readline library to allow the editing of keyboard input for any 
  command.

  **NOTE:** rlwrap currently won't compile. In order for this to work, one needs
            to patch OpenWRT as per [this pull request](https://github.com/openwrt/openwrt/pull/16445)

* [STklos](https://www.stklos.net/) is an R7RS Scheme implementation that
  includes a compiler for bytecode.

  **NOTE:** STklos depends on `libsigsegv` and `libgc`. There are `Makefile`s
            in this repository for building those too.
  
  **NOTE:** The same version of STklos needs to be locally installed.

* [TinyScheme](http://tinyscheme.sourceforge.net/home.html) maintained
  by Dimitrios Souflis, Kevin Cozens and Jonathan
  S. Shapiro is a lightweight Scheme interpreter that implements as
  large a subset of R5RS as was possible without getting very large and
  complicated. The package distributed here includes the TSX extension
  by Manuel Heras-Gilsanz, which provides support for operations with
  filesystem, sockets, date/time, and more. TinyScheme has the usual
  Lisp features (macros, homoiconicity and `eval`, first class functions,
  etc), and the TSX extension gives you:
    * Sockets
    * Filesystem access
    * Time functions
    * Access to environment variables
    * Execution of commands using /bin/sh
  Unfortunately, the souce code for TSX is not available from its author anymore
  (as far as I can see, the page is gone). But the last version I could get is
  [this one](tsx.tar.gz). It's small enough that a code review is actually easy.

  **NOTE:** please refer to Tinyscheme's [issues in Sourceforge](https://sourceforge.net/p/tinyscheme/_list/tickets)

  **NOTE:** [LibreCMC](https://librecmc.org/) already has Tinyscheme in their package feed (packaged by Christopher Howard).

  **NOTE:** There is a newer version of Tinyscheme available, but not yet packaged.

  Tinyscheme has been in development since 1998.

* [Twin](https://github.com/cosmos72/twin), a windowing environment with mouse support, window manager, terminal emulator 
  and networked clients, all inside a text display.

  **NOTE:** this is mostly done for fun; there are several warnings issued during compilation.
  The package, after installed on a MIPS big endian device, takes ~470k.

### Available, but likely won't be updated:

* [Foment Scheme](https://github.com/leftmike/foment) is a small R7RS 
  Scheme implementation which supports native threads, unicode, networking,
  and a modern memory management system.

  **NOTE:** it seems that Foment may crash on at least one MIPS 32bit little endian architecture.

  Currently a specific development snapshot is used. When
  a new release of Foment is out, it will be used.

* [PicoLisp](http://www.picolisp.com/) is a dialect of Lisp that is
  remarkably small and simple, and yet quite useful in
  practice. Besides the usual Lisp features (macros,
  homoiconicity and `eval`, first class functions, etc), PicoLisp
  also has these interesting features:
    * A database supporting transactions;
    * PicoLisp works as an application server framework (including an HTTP server);
    * An API for system access (date/time, filesystem, etc);
    * A logic programming engine (Pilog, which has the semantics of Prolog but the syntax of Lisp);
    * Object-orientation extensions;
    * Coroutines.
    
* [SigScheme](https://github.com/uim/sigscheme) is a R5RS Scheme interpreter for embedded use, supporting
  R5RS, part of R6RS and some SRFIs. Hygienic macros are not enabled.


### Currently working on

### Failed attempts (will eventually try again)

#### With failing Makefile in the `not-yet-working` directory

* [ECL](https://common-lisp.net/project/ecl/) (does not detect libatomic, but the included version won't compile)
* [Emacs](https://www.gnu.org/software/emacs/) (compilation failed with gcc complaining about some struct having incomplete type -- probably some `#include` is missing, but I had no time to debug that. Also, it seems that Emacs
  is not too easy to cross-compile -- see [this thread](https://lists.gnu.org/archive/html/emacs-devel/2010-12/msg00737.html)).)

#### Makefile not yet published

* [ABCL](https://common-lisp.net/project/armedbear/) (the only JVM available for LibreCMC/OpenWRT is [JamVM](http://jamvm.sourceforge.net/) -- but only [GNU Classpath](https://www.gnu.org/software/classpath/) is available on OpenWRT, and it does not support Java 1.6, which ABCL needs)
* [Bigloo](http://www-sop.inria.fr/indes/fp/Bigloo/index.html) -- haven't tried yet; it's actually a huge system. It also seems to not run on MIPS (but does run on ARM and on the JVM).
* [Clisp](http://clisp.org) tries to run the cross-compiled binary on the build machine (`./intparam intparam.h`, but `./intparam` is a compiled binary, for a different architecture))
* [Cyclone](https://justinethier.github.io/cyclone/) (neither [libck](http://concurrencykit.org/) nor [libtommath](https://github.com/libtom/libtommath) are available in LibreCMC or OpenWRT -- would have to package those first)
* [Gambit](http://gambitscheme.org/wiki/index.php/Main_Page) -- haven't tried yet; it's huge (comparable to Bigloo in size). I'm also not sure it runs on typical OpenWRT architectures.
* [Gforth](https://www.gnu.org/software/gforth/) (from the INSTALL file: *"There is currently no simple way to do cross-installation of Gforth (apart from Gforth EC). The current build process interleaves compiling and running heavily, so multiple transfers between build and target machine would be required."*) I have also tried compiling inside OpenWRT (in a router), but compilation failed.
* [GNU Prolog](http://www.gprolog.org)  `configure: error: unsupported architecture mips-openwrt-linux-gnu`
* [GNU Smalltalk](http://smalltalk.gnu.org/) Fixed several problems, then found that it compile-then-runs a binary... `./genprims < ./prims.def > prims.inl` -> tries to run a MIPS binary on the compiling machine, fails!
* [Guile](https://www.gnu.org/software/guile/) seems a bit complex to port, and it is also somewhat large, although I would really like to have it
  running!
* [PFE](http://pfe.sourceforge.net/) fails to compile -- it looks like PFE depends on a very outdated version of autotools...
* [Retro](http://forth.works/book.html) (the Makefile generates and uses binaries a couple of times; it would be necessary to write a script that calls `make` for a few of the targets, but does most of the work manually)
* [Sagittarius](https://github.com/ktakashi/sagittarius-scheme)
* [SBCL](http://sbcl.org/) needs several passes to run interleaved on host and target machine.
* [Scheme48](http://www.s48.org/) (`configure: error: failed to compile test program`, it tries to run the binary, wrong architecture -- need to find out how to disable that test)
* [Scheme 9 From Empty Space](https://t3x.org/s9fes/) (the image file generated on the build machine didn't work on the target; tried building later on the target, but
  anyway, the image is *huge* (5Mb), so I believe it wouldn't work for this purpose)
* [SCM](https://people.csail.mit.edu/jaffer/SCM) and [SLIB](https://people.csail.mit.edu/jaffer/slib/). SCM compiles on an ordinary desktop, but the linker complains about several missing symbols in the buildroot.
* [XLISP](https://github.com/dbetz/xlisp) probably works, but doesn't seem to be in a robust state (tried using it and it crashed a couple of times).
* [Yap](https://github.com/vscosta/yap-6.3) doesn't seem to support cross-compilation (the `configure` script is hand-tailored, and doesn't accept `--host` or `--target` options)
* [yForth?](https://tracker.debian.org/pkg/yforth) is portable, except for one thing: it needs to compile and run a small piece of code on the target machine in order to decide how to handle division.

Did not try:

* [Ciao](https://ciao-lang.org) may be interesting.
* [MIT Scheme](https://www.gnu.org/software/mit-scheme/).
* [XCL](http://armedbear.org/) needs some assembly to be written, but would be interesting. Besides that porting effort, the latest version needs some bug fixing.
* [XSB](http://xsb.sourceforge.net) may be interesting.
