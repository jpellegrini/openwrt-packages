include $(TOPDIR)/rules.mk

PKG_NAME:=stklos
PKG_VERSION:=1.60
STKLOS_VERSION:=1.60
PKG_RELEASE:=1

PKG_BUILD_DIR:=$(BUILD_DIR)/stklos-$(PKG_VERSION)
PKG_SOURCE:=stklos-$(PKG_VERSION).tar.gz
PKG_SOURCE_URL:=https://www.stklos.net/download/
PKG_HASH:=ed86a477fba2a7d1e84311063a284f9ea8ef617c7443539347ed494250c0e01d

PKG_FIXUP:=autoreconf

PKG_BUILD_DEPENDS += +libpthread +libffi +libpcre +libatomic +libgmp +libgc

include $(INCLUDE_DIR)/package.mk

define Package/stklos
SECTION:=lang
CATEGORY:=Languages
TITLE:=STklos
URL:=https://www.stklos.net/
DEPENDS:= +libpthread +libgmp +libatomic +libffi +libpcre +libgc
MAINTAINER:=Jeronimo Pellegrini <j_p@aleph0.info>
endef


define Package/stklos/description
  STklos is a free Scheme system compliant with the languages features defined in R7RS small. The aim of this implementation is to be fast as well as light. The implementation is based on an ad-hoc Virtual Machine.
  The salient points of STklos are

  *  an efficient and powerful object system based on CLOS providing
     -   Multiple Inheritance,
     -   Generic Functions,
     -   Multi-methods
     -   an efficient MOP (Meta Object Protocol)
  *  a simple to use module system
  *  a full tower of numbers implementation, as defined in R5RS,
  *  easy connection to the GTK+ toolkit,
  *  a Perl compatible regular expressions thanks to the PCRE package,
  *  easily extensible with its ScmPkg mechanism,
  *  it implements properly tail recursive calls.
endef


##  Build STklos locally before cross-compiling:
##
MAKE_FLAGS += COMP=$(PKG_BUILD_DIR)/build_install/usr/local/bin/stklos-compile
MAKE_FLAGS += STKLOS_BINARY=$(PKG_BUILD_DIR)/build_install/usr/local/bin/stklos
MAKE_FLAGS += GENLEX=$(PKG_BUILD_DIR)/build_install/usr/local/bin/stklos-genlex

define Build/Prepare
	$(call Build/Prepare/Default)
	$(INSTALL_DIR) $(TMPDIR)/$(PKG_NAME)/build_src
	$(CP) -r $(PKG_BUILD_DIR)/* $(TMPDIR)/$(PKG_NAME)/build_src
	$(INSTALL_DIR) $(PKG_BUILD_DIR)/build_install
	(unset STRIP M4 PKG_CONFIG_PATH ACLOCAL_INCLUDE TARGET_CXX_NOCACHE TARGET_CC_NOCACHE PKG_CONFIG_LIBDIR CONFIG_SITE && cd $(TMPDIR)/$(PKG_NAME)/build_src && ./configure )
	make -C $(TMPDIR)/$(PKG_NAME)/build_src
	LD_LIBRARY_PATH=$(PKG_BUILD_DIR)/build_install/usr/local/lib:$LD_LIBRARY_PATH make -C $(TMPDIR)/$(PKG_NAME)/build_src DESTDIR=$(PKG_BUILD_DIR)/build_install STRIP=strip install-base
	make -C $(TMPDIR)/$(PKG_NAME)/build_src distclean
endef
##
## Now a local STklos is installed on $(PKG_BUILD_DIR)/build_install


## DEBUGGING:
##
# to debug STklos on routers or qemu virtual machines, enable -g
# and -O0.
#TARGET_CFLAGS += -g -O0
#
# however, musl does not support execinfo.h, so we cannot use
# -DSTK_DEBUG
##

## When compiling, use the stklos-compile binary we have just prepared locally:
##
define Build/Compile
	$(call Build/Compile/Default,COMP=$(PKG_BUILD_DIR)/build_install/usr/local/bin/stklos-compile)
endef
##


## A simple install target.
##
define Package/stklos/install
	## install STKlos. We do install-base, which installs only the minimum necessary
	## for STklos to run (but it does install modules and SRFIs). there will
	## be no online help.
	## to get a full-fleged STklos system, change "install-base" below to "install".
	make -C $(PKG_BUILD_DIR) $(MAKE_FLAGS) DESTDIR=$(1) install-base
	#
	## if you'd like to run the tests on the device:
	#$(INSTALL_DIR)  $(1)/usr/share/stklos/$(STKLOS_VERSION)/tests
	#$(CP)  $(PKG_BUILD_DIR)/tests/* $(1)/usr/share/stklos/$(STKLOS_VERSION)/tests/
	## then, on the device, do this:
	# cd stklos /usr/share/stklos/1.50 && --no-init-file --utf8-encoding=yes -f do-test.stk
	## note: tests use a lot of memory!
	#
	## Remove some binaries that we don't want -- this is likely to depend on individual
	## needs!
	rm $(1)/usr/bin/stklos-genlex
	rm $(1)/usr/bin/stklos-pkg
	rm $(1)/usr/bin/stklos-config
	rm $(1)/usr/bin/stklos-compile
	rm -rf $(1)/usr/include/stklos
	#
	rm -rf $(1)/usr/share/stklos/$(STKLOS_VERSION)/ScmPkg.d/
	rm -rf $(1)/usr/share/stklos/$(STKLOS_VERSION)/scmpkg-support.ostk
	rm -rf $(1)/usr/share/stklos/$(STKLOS_VERSION)/etc
	#
	rm -rf $(1)/usr/share/stklos/$(STKLOS_VERSION)/*.stk        # sources
	rm -rf $(1)/usr/share/stklos/$(STKLOS_VERSION)/help.ostk    # useless without DOCDB
	#
	## remove some large libraries:
	rm $(1)/usr/share/stklos/$(STKLOS_VERSION)/bigmatch.ostk    # bigloo's match case/lambda
	rm $(1)/usr/share/stklos/$(STKLOS_VERSION)/lalr.ostk
	rm $(1)/usr/share/stklos/$(STKLOS_VERSION)/lex-rt.ostk
	rm $(1)/usr/share/stklos/$(STKLOS_VERSION)/compfile.ostk    # compile file
	rm $(1)/usr/share/stklos/$(STKLOS_VERSION)/recette.ostk     # bigloo snow recette
	rm $(1)/usr/share/stklos/$(STKLOS_VERSION)/http.ostk        # use OpenWRT's uhttpd
	rm $(1)/usr/share/stklos/$(STKLOS_VERSION)/full-syntax.ostk # large, let's try not to use it
	rm $(1)/usr/share/stklos/$(STKLOS_VERSION)/srfi-64.ostk     # test lib, won't use on the device
	rm $(1)/usr/share/stklos/$(STKLOS_VERSION)/srfi-113.ostk    # sets & bags
	rm $(1)/usr/share/stklos/$(STKLOS_VERSION)/srfi-189.ostk    # maybe & either
	rm $(1)/usr/share/stklos/$(STKLOS_VERSION)/srfi-128.ostk    # comparators
	rm $(1)/usr/share/stklos/$(STKLOS_VERSION)/srfi-130.ostk    # cursor based string lib
	#
	## no need to strip -- the STklos install target already does it!
endef

$(eval $(call BuildPackage,stklos))
