diff -Nur picoLisp/Makefile picoLisp-new/Makefile
--- picoLisp/Makefile	1969-12-31 21:00:00.000000000 -0300
+++ picoLisp-new/Makefile	2019-03-09 13:49:59.085644637 -0300
@@ -0,0 +1,6 @@
+all:
+	(cd src; make)
+
+clean:
+	(cd src; make clean)
+
diff -Nur picoLisp/src/Makefile picoLisp-new/src/Makefile
--- picoLisp/src/Makefile	2017-10-04 04:22:44.000000000 -0300
+++ picoLisp-new/src/Makefile	2019-03-09 13:48:38.061182919 -0300
@@ -6,11 +6,16 @@
 
 picoFiles = main.c gc.c apply.c flow.c sym.c subr.c big.c io.c net.c tab.c
 
-CC = gcc
+#CC = gcc
 # CCLD is the cc (compiler frontend) to use for the link step.
-CCLD = gcc
+#CCLD = gcc
 
-M32=-m32
+# Honor the $(CC) variable, inherited form the environment (it's essential
+# when cross-compiling)
+CCLD = $(CC)
+
+#M32=-m32
+M32=-fPIC
 
 CFLAGS = -c -O2 -pipe \
 	-falign-functions=32 -fomit-frame-pointer -fno-strict-aliasing \
