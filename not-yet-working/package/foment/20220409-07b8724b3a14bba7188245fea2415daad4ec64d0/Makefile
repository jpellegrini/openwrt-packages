include $(TOPDIR)/rules.mk

FOMENT_VERSION:=07b8724b3a14bba7188245fea2415daad4ec64d0
FOMENT_DATE:=20220409

PKG_NAME:=foment-scheme
PKG_VERSION=$(FOMENT_DATE)-$(FOMENT_VERSION)
PKG_RELEASE:=1

PKG_BUILD_DIR := $(BUILD_DIR)/foment-$(FOMENT_VERSION)
PKG_SOURCE:=$(FOMENT_VERSION).zip
PKG_SOURCE_URL:=https://github.com/leftmike/foment/archive/
PKG_HASH:=0c78f353864236cc20ac2295cc10130a4bf4c81774bff1c945e5b0d97814360a

SUBDIR=unix

include $(INCLUDE_DIR)/uclibc++.mk
include $(INCLUDE_DIR)/package.mk

define Package/foment-scheme
SECTION:=lang
CATEGORY:=Languages
TITLE:=Foment
DEPENDS:=+libpthread +uclibcxx
URL:=https://github.com/leftmike/foment
MAINTAINER:=Jeronimo Pellegrini <j_p@aleph0.info>
endef

define Package/foment-scheme/description
Foment is an R7RS Scheme implementation wich supports native threads,
full Unicode, networking, and some SRFIs (1/list library, 60/integers as bits,
106/basic socket interface, 111/boxes, 112/environment enquiry, 124/ephemerons,
128/comparators, and 181/custom ports).
endef

TARGET_LDFLAGS += -luClibc++

ifeq ($(CONFIG_BIG_ENDIAN),y)
beinfo: $(info *** Big endian detected, look for -DFOMENT_BIG_ENDIAN=0 ***)
TARGET_CONFIGURE_OPTS+= FOMENT_BIG_ENDIAN=1
endif

define Build/Compile
	$(MAKE) -C $(PKG_BUILD_DIR)/unix $(TARGET_CONFIGURE_OPTS)
	rm $(PKG_BUILD_DIR)/unix/debug/txt2cpp 
endef


define Package/foment-scheme/install
	$(INSTALL_DIR) $(1)/usr/bin
	cp $(PKG_BUILD_DIR)/unix/debug/foment $(1)/usr/bin/
	$(STRIP) $(1)/usr/bin/foment
endef

$(eval $(call BuildPackage,foment-scheme,+uclibcxx))
