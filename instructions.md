% Compiling packages for OpenWRT -- instructions

## Get the Makefile tarball ##

From this site, get the tarball with the Makefile.
It is something like `tinyscheme-x.zz-openwrt-w.tar.gz` or `picolisp-x.zz-openwrt-w.tar.gz`,
and will be under the `VERSION` directory, where `VERSION` is the OpenWRT
version used. 

**note:** for now, the version in this repository will always be "1",
since this is mostly an experiment. I will come up with a different 
versioning scheme later.

For example,

    18.06.2/chicken-scheme-5.0.0-openwrt-1.tar.gz

is the tarball for Chicken Scheme version `5.0.0`;
the `openwrt-1` means it's the first vresion of the OpenWRT `Makefile`.


Alternatively, you can just checkout the git repository and copy the
subtree for the language you want.

## Get OpenWRT ##

For trunk (bleeding edge):

    git clone https://git.openwrt.org/openwrt/openwrt.git

For the stable release, get the tags from that git repository and
checkout the release you want.

NOTE: if you are trying to compile against trunk and it doesn't work,
try compiling against the source of the latest release. Even if you installed
a development snapshot, the package compiled against the latest release
*may* work.


Then,

    cd openwrt
    tar xzf tinyscheme-x.zz-openwrt-w.tar.gz
    tar xzf picolisp-x.zz-openwrt-w.tar.gz

From here on, `LANG` is the package name.

Look at the `package/LANG/Makefile`. It automatically
downloads the language implementation.


## Configure and build ##

From the OpenWRT sources, do:

    make menuconfig
choose to build for your device; confgure packages: choose to build `languages->LANG`

Now download and compile the toolchain (this takes some gigabytes!)

    make tools/install
    make toolchain/install
    scripts/feeds update

And finally, compile the package:

    make package/LANG/compile

The package will be placed in `bin/TARGET/packages/LANG-....ipk`


Send it to your router and install it with opkg:

    # opkg install LANG-....ipk

## Reporting bugs

Open an issue on gitlab!

If compilation breaks, try

    make package/LANG/compile -j1 V=s

to see the build output.


## Including the language in a full OpenWRT image

If you want the package to be included in the OpenWRT image,
then when you run make manuconfig you should select them
not as modules ("M"), but as built-in ("*").

Then do:

    make target/linux/clean world

The above command will build a full OpenWRT image with `LANG`
included:

    bin/TARGET/openwrt-ARCH-le-vmlinux.elf

You can then either flash your device with this image or run it 
under [qemu](https://www.qemu.org/).

